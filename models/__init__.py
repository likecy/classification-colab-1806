'''
Author: 陈云
Date: 2021-04-12 18:53:46
LastEditTime: 2021-07-11 21:52:21
LastEditors: 陈云
Description: 
FilePath: /classification/models/__init__.py
松子落，有鹿来
'''
import os
# home ="./"
## 预训练模型的本地存放位置
# LOCAL_PRETRAINED = {
#     'resnext101_32x8d': home + '/weights/resnext101_32x8.pth',
#     'resnext101_32x16d': home + '/weights/resnext101_32x16.pth',
#     'resnext101_32x48d': home + '/weights/resnext101_32x48.pth',
#     'resnext101_32x32d': home + '/weights/resnext101_32x32.pth',
#     'resnet50': home +'/weights/resnet50.pth',
#     'resnet101': home +'/weights/resnet101.pth',
#     'densenet121': home +'/weights/densenet121.pth',
#     'densenet169': home +'/weights/densenet169.pth',
#     'moblienetv2': home +'/weights/mobilenetv2.pth',
#     'efficientnet-b7': home + '/weights/efficientnet-b7.pth',
#     'efficientnet-b8': home + '/weights/efficientnet-b8.pth',
#     'efficientnet-b0': home+'/weights/efficientnet-b0.pth'
# }


from .vision import *
from .resnext_wsl import *
from .efficientnet_pytorch import *
from .build_model import *
