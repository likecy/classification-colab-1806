# classification-colab-1806

#### 介绍
西南交通大学-1806实验室-图片分类任务（pytorch），目前支持网络结构如下：

```
MODEL_NAMES ={
    'alexnet': Alexnet,
    'googlenet':Googlenet,
    'resnet18':Resnet18,
    'resnet50': Resnet50,
    'resnet101': Resnet101,
    'resnet152':Resnet152,
    'resnext101_32x8d': Resnext101_32x8d,
    'resnext101_32x16d': Resnext101_32x16d,
    'resnext101_32x48d': Resnext101_32x48d,
    'resnext101_32x32d': Resnext101_32x32d,
    'densenet121': Densenet121,
    'densenet169': Densenet169,
    'moblienetv2': Mobilenetv2,
    'squeezenet1_0':Squeezenet1_0,
    'squeezenet1_1':Squeezenet1_1,
    'shufflenet_v2_x0_5':Shufflenet_v2_x0_5,
    'shufflenet_v2_x1_0':Shufflenet_v2_x1_0,
    'shufflenet_v2_x1_5':Shufflenet_v2_x1_5,
    'shufflenet_v2_x2_0':Shufflenet_v2_x2_0,
    'efficientnet-b7': Efficientnet,
    'efficientnet-b0': Efficientnet,
    'efficientnet-b8': Efficientnet
}
```



#### 软件架构
软件架构说明

```bash
├── README.md
├── data_iter   #数据处理
│   ├── __pycache__
│   └── datasets.py
├── datasets  #数据目录
│   └── datasets.txt
├── imagenet
│   ├── imagenet_msg.json
│   └── read_imagenet_msg.py
├── inference.py  #推断
├── loss       # 损失函数
│   ├── __pycache__
│   └── loss.py
├── model-epoch-inference.py  #模型批量测试预测
├── models                   # 网络模型
│   ├── __init__.py
│   ├── __pycache__
│   ├── build_model.py
│   ├── efficientnet_pytorch  #第三方网络案例
│   ├── resnext_wsl.py
│   └── vision          # 模型包
├── read_datasets.py   # 数据预处理
├── requirements.txt   #环境依赖
├── train.py          #训练入口，注意训练参数配置
├── utils             #工具包
│   ├── __pycache__
│   ├── common_utils.py
│   └── model_utils.py
└── weights        # 权重

```




#### 安装教程

1.  安装环境依赖：pip install -r requirements.txt

#### 使用说明

1. 训练：python train.py # 注意相关参数配置

2. 推断 ：python inference.py # 注意相关参数配置

3. tensorbordX 训练日志可视化：tensorboard --logdir ./logs_dir 

   

#### 参与贡献

西南交通大学-1806实验室-陈云

2021-07-23

